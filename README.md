*****Container Orchestration with Kubernetes*****

***Demo Project***

Deploy Mosquitto message broker with ConfigMap and
Secret Volume Types

***Technologies Used***

Kubernetes, Docker, Mosquitto

***Project Description***

Define configuration and passwords for Mosquitto
message broker with ConfigMap and Secret Volume
types